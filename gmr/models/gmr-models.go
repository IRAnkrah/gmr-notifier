package models

type GMRPlayer struct {
	Id        int64  `json:"SteamID"`
	Name      string `json:"PersonaName"`
	AvatarUrl string `json:"AvatarUrl"`
	Status    int32  `json:"PersonaState"`
	GameId    int32  `json:"GameID"`
}

type GMRCurrentTurn struct {
	Id           int32  `json:"TurnId"`
	Number       int32  `json:"Number"`
	PlayerId     int64  `json:"UserId"`
	Started      string `json:"Started"`
	Expires      string `json:"Expires"`
	Skipped      bool   `json:"Skipped"`
	PlayerNumber int32  `json:"PlayerNumber"`
}

type GMRGame struct {
	Name    string `json:"Name"`
	GameId  int32  `json:"GameId"`
	Players []struct {
		Id        int64 `json:"UserId"`
		TurnOrder int32 `json:"TurnOrder"`
	} `json:"Players"`
	CurrentTurn GMRCurrentTurn `json:"CurrentTurn"`
}

type GMRGamesAndPlayers struct {
	Games   []GMRGame   `json:"Games"`
	Players []GMRPlayer `json:"Players"`
	Points  int32       `json:"CurrentTotalPoints"`
}

// api ref http://multiplayerrobot.com/About/api

package gmr

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gmr-notifer/gmr/models"
)

var apiRoot string = "http://multiplayerrobot.com/api/Diplomacy"

type queryCache struct {
	gamesAndPlayers models.GMRGamesAndPlayers
	queryHash       uint32
}

var cache queryCache

func hashQuery(s string) uint32 {
	s += time.Now().Format("01-02-2006 15:04:05")

	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

func AuthernticateUser(authKey string) string {
	resp, err := http.Get(fmt.Sprintf("%s/AuthenticateUser?authKey=%s", apiRoot, authKey))
	if err != nil {
		// TODO: handle
		return ""
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// TODO: handle
		return ""
	}

	return string(body)
}

func GetGamesAndPlayers(authKey string, playerIds []int64) (models.GMRGamesAndPlayers, error) {

	// create player id string
	var playerIdStr string
	for idx, playerId := range playerIds {
		if idx > 0 {
			playerIdStr += "_"
		}
		playerIdStr += strconv.FormatInt(playerId, 10)
	}

	query := fmt.Sprintf("%s/GetGamesAndPlayers?playerIDText=%s&authKey=%s", apiRoot, playerIdStr, authKey)

	// use hash to not spam api with the same query over and over.
	queryHash := hashQuery(query)
	if cache.queryHash == queryHash {
		return cache.gamesAndPlayers, nil
	}

	resp, err := http.Get(query)
	if err != nil {
		fmt.Printf(err.Error())
		return models.GMRGamesAndPlayers{}, fmt.Errorf("%s failed", query)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf(err.Error())
		return models.GMRGamesAndPlayers{}, fmt.Errorf("could not read responce body")
	}

	// uncomment if you want to write the content to a file for development purposes
	//os.WriteFile("sample-responce.json", body, fs.ModeIrregular)

	var gamesAndPlayers models.GMRGamesAndPlayers

	err = json.Unmarshal(body, &gamesAndPlayers)
	if err != nil {
		return models.GMRGamesAndPlayers{}, fmt.Errorf("Could not parse responce json")
	}

	cache.gamesAndPlayers = gamesAndPlayers
	cache.queryHash = queryHash

	return gamesAndPlayers, nil
}

func GetCurrentTurn(authKey string, gameId int32) (models.GMRCurrentTurn, error) {
	gamesAndPlayers, err := GetGamesAndPlayers(authKey, []int64{})
	if err != nil {
		return models.GMRCurrentTurn{}, err
	}

	for _, game := range gamesAndPlayers.Games {
		if game.GameId == gameId {
			return game.CurrentTurn, nil
		}
	}

	return models.GMRCurrentTurn{}, fmt.Errorf("No matching game found")
}

// this is a test method to be used when you don't want to hit their servers for data.
// func ReadGamesAndPlayers() models.GMRGamesAndPlayers {
// 	data, err := os.ReadFile("sample-responce.json")
// 	if err != nil {
// 		fmt.Printf(err.Error())
// 		return models.GMRGamesAndPlayers{}
// 	}

// 	var gamesAndPlayers models.GMRGamesAndPlayers

// 	err = json.Unmarshal(data, &gamesAndPlayers)
// 	if err != nil {
// 		fmt.Printf(err.Error())
// 		return models.GMRGamesAndPlayers{}
// 	}

// 	return gamesAndPlayers
// }

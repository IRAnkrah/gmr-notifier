package discord

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func Init(token string) (discordBot *discordgo.Session, err error) {
	discordBot, err = discordgo.New("Bot " + token)
	if err != nil {
		return nil, err
	}

	err = discordBot.Open()
	if err != nil {
		return nil, err
	}

	fmt.Println("bot is now running")
	return discordBot, nil
}

func Deinint(discordBot *discordgo.Session) {
	discordBot.Close()
}

func BroadcastPlayerTurn(discordBot *discordgo.Session, discordId int64, channelId string) error {

	msg := fmt.Sprintf("it is your turn <@%d>", discordId)

	_, err := discordBot.ChannelMessageSend(channelId, msg)

	return err
}

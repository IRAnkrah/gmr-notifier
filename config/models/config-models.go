package models

type DiscordConfig struct {
	BotToken         string          `json:"bot_token"`
	ChannelId        string          `json:"channel_id"`
	SteamToDiscordID map[int64]int64 `json:"steam_to_discord_id"`
}

type Config struct {
	RefreshTime   int32            `json:"refresh_time"`
	AuthKey       string           `json:"auth_key"`
	GameIds       []int32          `json:"game_ids"`
	Players       map[int64]string `json:"players"`
	DiscordConfig DiscordConfig    `json:"discord_config"`
}

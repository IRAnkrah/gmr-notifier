package config

import (
	"encoding/json"
	"os"

	"gmr-notifer/config/models"
)

func GetConfig(path string) (models.Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return models.Config{}, err
	}

	var config models.Config
	err = json.Unmarshal(data, &config)
	if err != nil {
		return models.Config{}, err
	}

	return config, nil
}

package cache

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gmr-notifer/cache/models"
)

var cachePath string = "/cache/cache.json"

//var cachePath string = "cache.json"

func ReadCurrentPlayerId() (map[int32]int64, error) {
	if _, err := os.Stat(cachePath); os.IsNotExist(err) {
		CacheCurrentPlayerId(map[int32]int64{})
	}

	data, err := os.ReadFile(cachePath)
	if err != nil {
		return map[int32]int64{}, err
	}

	var cache models.Cache
	err = json.Unmarshal(data, &cache)
	if err != nil {
		return map[int32]int64{}, err
	}

	return cache.CurrentPlayerId, nil
}

func CacheCurrentPlayerId(currentPlayerId map[int32]int64) error {
	cache := models.Cache{CurrentPlayerId: currentPlayerId}
	data, err := json.MarshalIndent(cache, "", " ")

	if err == nil {
		err = ioutil.WriteFile(cachePath, data, 0644)
	}

	return err
}

package main

import (
	"fmt"
	"gmr-notifer/cache"
	"gmr-notifer/config"
	"gmr-notifer/gmr"
	"gmr-notifer/notifier/discord"
	"time"
)

func main() {
	config, err := config.GetConfig("/config/config.json")
	//config, err := config.GetConfig("config.json")
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	var abort bool = false
	currentPlayerId, err := cache.ReadCurrentPlayerId()
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	discordBot, err := discord.Init(config.DiscordConfig.BotToken)
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	for !abort {
		for _, gameId := range config.GameIds {
			currentTurn, err := gmr.GetCurrentTurn(config.AuthKey, gameId)
			if err != nil {
				//abort = true
				fmt.Print(err.Error())
			} else if currentTurn.PlayerId != currentPlayerId[gameId] {
				currentPlayerId[gameId] = currentTurn.PlayerId
				cache.CacheCurrentPlayerId(currentPlayerId)

				discord.BroadcastPlayerTurn(discordBot, config.DiscordConfig.SteamToDiscordID[currentTurn.PlayerId], config.DiscordConfig.ChannelId)
			}
		}
		time.Sleep(time.Duration(config.RefreshTime) * time.Minute)
	}

	discord.Deinint(discordBot)
}
